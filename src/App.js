import * as React from "react";
import '@coreui/coreui/dist/css/coreui.min.css'
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";

import LuckyGame from "./pages/lucky_game/LuckyGame";
import PlayGame from "./pages/lucky_game/PlayGame";
import ResultGame from "./pages/lucky_game/ResultGame";
import Test from "./pages/lucky_game/Test";

function App() {
  return (
    <React.StrictMode>
      <Router>
        <div className="App">
          <Switch>
            <Route path="/test"><Test /></Route>
            <Route path="/result_game"><ResultGame /></Route>
            <Route path="/lucky_game"><PlayGame /></Route>
            <Route exact path="/"><LuckyGame /></Route>
          </Switch>
        </div>
      </Router>
    </React.StrictMode>
  );
}

export default App;