import axiosClient from "./axiosClient";

export const getCustomers = (payload) => {
    return axiosClient({
        url: "/customers",
        method: "POST",
        data: payload
    })
}

export const getCustomersWinner = (payload) => {
    return axiosClient({
        url: "/customers_winner",
        method: "POST",
        data: payload
    })
}

export const updateStatus = (payload) => {
    return axiosClient({
        url: "/update_status",
        method: "POST",
        data: payload
    })
}

export const updateStatusDelete = (payload) => {
    return axiosClient({
        url: "/update_status_delete",
        method: "POST",
        data: payload
    })
}

export const updateStatusAll = (payload) => {
    return axiosClient({
        url: "/update_status_all",
        method: "POST",
        data: payload
    })
}

export const updateStatusReceived = (payload) => {
    return axiosClient({
        url: "/update_status_received",
        method: "POST",
        data: payload
    })
}
