import React from 'react'
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import { useOnKeyPress } from '../../hooks/useOnKeyPress';
import ReactHowler from 'react-howler';

function LuckyGame() {
    const url1 = `${process.env.PUBLIC_URL}/images/Backp.mp3`;
    const history = useHistory();
    const pushNext = () => {
        history.push('/lucky_game');
    }
    useOnKeyPress(pushNext, 'n');

    const styleButton = {
        width: "500px",
        height: '500px',
        borderRadius: "10px",
        border: "none",
        fontWeight: "bold",
        color: "black",
        fontSize: "20px",
    }

    const startGame = () => {
        history.push("/lucky_game");
    }
    const url = `${process.env.PUBLIC_URL}/images/backstart.mp4`;
    return (
        <div className="video-background">
            <video autoPlay loop muted>
                <source src={url} type="video/mp4" />
                Your browser does not support the video tag.
            </video>
            {/* Đặt nội dung khác của bạn ở đây, ví dụ: */}
            <div className="content" style={styleButton} onClick={startGame}>
                <ReactHowler
                    src={url1}
                    playing={true}
                    loop={true}
                />
            </div>
        </div>
    )
}

export default LuckyGame
