import React, { useEffect, useState } from 'react'
import { getCustomersWinner, updateStatusReceived } from '../../api/customerApi'
import { CCard, CCardBody, CCardHeader, CCardTitle, CCol, CContainer, CListGroup, CListGroupItem, CRow } from '@coreui/react';
import { useOnKeyPress } from '../../hooks/useOnKeyPress';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import ReactHowler from 'react-howler';

function ResultGame() {
    const urlfinal = `${process.env.PUBLIC_URL}/images/traothuong.mp3`;;
    const history = useHistory();
    const pushPlayGame = () => {
        history.push("/lucky_game");
    }
    useOnKeyPress(pushPlayGame, "p");
    const [playMusic, setPlayMusic] = useState(false);
    const [loading, setLoading] = useState(false);
    const [result, setResult] = useState([]);
    const [listmanv, setListManv] = useState([]);
    const [double, setDouble] = useState(false);

    const getAllResult = async () => {
        setLoading(true);
        const customers = await getCustomersWinner();
        if (customers.data.customers) {
            //để hiển thị 1 dòng hay 2 dòng
            if (customers.data.customers.length > 10) {
                setDouble(true);
            }
            setResult(customers.data.customers);
            let temp = [];
            customers.data.customers.forEach((item, index) => {
                temp.push(item.manv);
            })
            setListManv(temp);
            setLoading(false);
        }
    }

    const updateDoneRecevied = async () => {

        const update = await updateStatusReceived({ listmanv: listmanv });

        if (update.data.success === 'success') {
            console.log('update da nhan qua thanh cong');
        } else {
            console.log('Co loi update nhan qua vui long thu lai !');
        }
        history.push("/lucky_game");
    }

    useOnKeyPress(updateDoneRecevied, "k");

    useEffect(() => {
        setPlayMusic(true);
    }, [result])
    useEffect(() => {
        getAllResult();
    }, []);

    const styleBac = {
        backgroundImage: `url(${process.env.PUBLIC_URL}/images/backend.jpg)`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        height: '100vh',
        // display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',


    }

    return (
        <div className='full-screen-background' style={styleBac}>
            <ReactHowler
                src={urlfinal}
                playing={playMusic}
                loop={true}
            />
            <CCardHeader>
                <CCardTitle style={{ fontSize: '65px', color: 'yellow', marginBottom: "40px", paddingTop: '50px' }}>
                    Danh sách người trúng thưởng
                </CCardTitle>
            </CCardHeader>
            {
                double ? (
                    <CRow className='fix_result'>
                        <CCol className='col_result-1' >
                            <CListGroup className='list-group'>
                                {
                                    result.length > 0 && result.map((item, index) => {
                                        return (
                                            index + 1 <= Object.keys(result).length / 2 && (

                                                <CListGroup className='list-group'>
                                                    <CListGroupItem
                                                        key={item.manv}
                                                        style={{ backgroundColor: 'transparent', border: 'none', color: 'yellow' }} >
                                                        {/* {item.manv.toUpperCase() + " - " + item.hoten.toUpperCase() + " - " + item.bophan.toUpperCase() + " - " + item.bp_compact.toUpperCase()} */}
                                                        {index + 1 + ". " + item.hoten.toUpperCase() + " - " + item.bophan.toUpperCase() + " - " + item.bp_compact.toUpperCase()}
                                                    </CListGroupItem>
                                                </CListGroup>
                                            )
                                        )
                                    })
                                }
                            </CListGroup>
                        </CCol>
                        <CCol className='col_result' >
                            <CListGroup className='list-group'>
                                {
                                    result.length > 0 && result.map((item, index) => {
                                        return (
                                            index + 1 > Object.keys(result).length / 2 && (

                                                <CListGroup className='list-group' >
                                                    <CListGroupItem
                                                        key={item.manv}
                                                        style={{ backgroundColor: 'transparent', border: 'none', color: 'yellow' }} >
                                                        {/* {item.manv.toUpperCase() + " - " + item.hoten.toUpperCase() + " - " + item.bophan.toUpperCase() + " - " + item.bp_compact.toUpperCase()} */}
                                                        {index + 1 + ". " + item.hoten.toUpperCase() + " - " + item.bophan.toUpperCase() + " - " + item.bp_compact.toUpperCase()}
                                                    </CListGroupItem>
                                                </CListGroup>
                                            )
                                        )
                                    })
                                }
                            </CListGroup>
                        </CCol>
                    </CRow>
                ) : (
                    <CRow className='fix_result1'>
                        <CCol >
                            <CListGroup className='list-group'>
                                {
                                    result.length > 0 && result.map((item, index) => {
                                        return (
                                            <CListGroup className='list-group' >
                                                <CListGroupItem
                                                    key={item.manv}
                                                    style={{ backgroundColor: 'transparent', border: 'none', color: 'yellow' }} >
                                                    {/* {item.manv.toUpperCase() + " - " + item.hoten.toUpperCase() + " - " + item.bophan.toUpperCase() + " - " + item.bp_compact.toUpperCase()} */}
                                                    {index + 1 + ". " + item.hoten.toUpperCase() + " - " + item.bophan.toUpperCase() + " - " + item.bp_compact.toUpperCase()}
                                                </CListGroupItem>
                                            </CListGroup>
                                        )
                                    })
                                }
                            </CListGroup>
                        </CCol>
                    </CRow>
                )
            }
            <div
                style={{
                    position: "fixed",
                    bottom: "0%",
                    left: "0%",
                    width: "100px",
                    height: "100px",
                }}
                onClick={() => { history.push("/lucky_game") }}

            >
                {/* prew */}
            </div>
            <div
                style={{
                    position: "fixed",
                    bottom: "0%",
                    right: "0%",
                    width: "100px",
                    height: "100px",

                }}
                onClick={updateDoneRecevied}

            >
            </div>
        </div>

    )
}

export default ResultGame
