import React, { useEffect, useRef, useState } from 'react'
import { DotLoader } from 'react-spinners';
import { getCustomers, updateStatus, updateStatusAll, updateStatusDelete } from '../../api/customerApi';
import TextLoop from "react-text-loop";
import Confetti from 'react-dom-confetti';
import { TypeAnimation } from 'react-type-animation';
import ReactHowler from 'react-howler';
import { useOnKeyPress } from '../../hooks/useOnKeyPress';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import { CFormCheck } from '@coreui/react';

function PlayGame() {

    const history = useHistory();
    const url = `${process.env.PUBLIC_URL}/images/backplay.mp4`;
    const url1 = `${process.env.PUBLIC_URL}/images/Quayso.mp3`;
    const url2 = `${process.env.PUBLIC_URL}/images/Backp.mp3`;
    const buttonRef = useRef(null);

    const [textButton, setTextButton] = useState("Start Now");
    const [loading, setLoading] = useState(false);
    const [display, setDisplay] = useState("");
    const [labelCss, setLabelCss] = useState({});

    const [listName, setListName] = useState([]);

    const [listWinner, setListWinner] = useState([]);
    const [listWinnerFinal, setListWinnerFinal] = useState([]);

    const [manv, SetManv] = useState("");
    const [hoten, SetHoten] = useState("");

    // State để theo dõi giá trị được chọn
    const [selectedValue, setSelectedValue] = useState(1);
    const [counter, setCounter] = useState(0);

    const [intervalLoop, setIntervalLoop] = useState(false);

    //checkbox
    const [isChecked, setIsChecked] = useState(false);

    const [playMusic, setPlayMusic] = useState(false);

    // Mảng các mục trong dropdown
    const options = [1, 2, 3, 4, 5];
    const handleSelectChange = (e) => {
        setSelectedValue(e.target.value);
        setCounter(0);
        setIntervalLoop(true);
    };


    function getRandomNumber(min, max, numberRandom) {
        let uniqueNumbers = [];
        while (uniqueNumbers.length < numberRandom) {

            const randomNumber = Math.floor(Math.random() * (max - min) + min);
            // Kiểm tra xem số ngẫu nhiên đã tồn tại trong mảng chưa
            if (!uniqueNumbers.includes(randomNumber)) {
                uniqueNumbers.push(randomNumber);
            }
        }
        return uniqueNumbers;
    }
    //stylecss
    const config = {
        angle: 90,
        spread: 360,
        startVelocity: 40,
        elementCount: 70,
        dragFriction: 0.12,
        duration: 3000,
        stagger: 3,
        width: "10px",
        height: "10px",
        perspective: "500px",
        colors: ["#a864fd", "#29cdff", "#78ff44", "#ff718d", "#fdff6a"]
    };
    const labelStyle = {
        position: "absolute",
        fontSize: "32px",
        fontWeight: "bold",
        border: "3px solid yellow",
        borderRadius: "5px",
        height: "120px",
        width: "1200px",

    }
    const styleButton = {
        outLine: "none",
        width: "220px",
        height: "60px",
        borderRadius: "10px",
        border: "none",
        fontWeight: "bold",
        color: "Yellow",
        backgroundColor: "#B21110",
        fontSize: "20px",
        position: "absolute",
        top: "95%",
        left: "10%",

    }

    const styleCustomer = {
        position: 'absolute',
        fontSize: "23px",
        fontWeight: "bold",
        width: "1200px",
    }
    const styleSelectBox = {
    }
    //end style css


    const getAllCustomers = async () => {
        const customers = await getCustomers();
        if (customers.data) {
            return customers.data;
        }
    }



    const startAndStopGame = async () => {
        setPlayMusic(true);
        setIntervalLoop(true);
        if (!loading) {
            setLoading(true);
            setTextButton("Stop");
            setLabelCss(labelStyle);
            const data = await getAllCustomers();
            if (data.customers.length) {
                const listIdRandom = getRandomNumber(0, data.customers.length, selectedValue);
                let temp = [];
                setDisplay("XIN CHÚC MỪNG ANH/CHỊ CBNV CÓ MÃ SỐ");
                for (let i = 0; i < listIdRandom.length; i++) {
                    // SetManv(data.customers[listIdRandom[0]].manv.toUpperCase());
                    SetHoten(data.customers[listIdRandom[0]].hoten.toUpperCase() + " - " + data.customers[listIdRandom[0]].bophan.toUpperCase() + " - " + data.customers[listIdRandom[0]].bp_compact.toUpperCase());
                    temp.push(data.customers[listIdRandom[i]]);
                    setListWinner(temp);
                }
            }
        } else {
            setTextButton("Start Now");
            let temp = [];
            listWinner.forEach((item, index) => {
                temp.push(item.manv.trim());
            })
            const update = await updateStatusAll({ listmanv: temp });
            if (update.data.success === 'success') {
                console.log('update success');
            } else {
                console.log('Co loi xay ra vui long thu lai!');
            }
        }
    }


    useEffect(async () => {
        const data = await getCustomers();
        let temp = [];
        data.data.customers.forEach((item, index) => {
            temp.push(item.hoten.toUpperCase());
        })
        setListName(temp);
    }, [loading])


    const getLoading = () => {
        if (textButton === 'Start Now' && loading) {
            setLoading(false);
            let count = 0;
            if (count + 1 === selectedValue) {
                setIntervalLoop(false);
                setPlayMusic(false);
                return;
            }
            const intervalId = setInterval(() => {
                if (loading) {
                    setLoading(false);
                }
                ++count;
                if (listWinner[count]) {
                    setDisplay("XIN CHÚC MỪNG ANH/CHỊ CBNV CÓ MÃ SỐ");
                    // SetManv(listWinner[count].manv.toUpperCase());
                    SetHoten(listWinner[count].hoten.toUpperCase() + " - " + listWinner[count].bophan.toUpperCase() + " - " + listWinner[count].bp_compact.toUpperCase());
                }
                if (count === Number(selectedValue)) {
                    setPlayMusic(false);
                    setIntervalLoop(false);
                    clearInterval(intervalId);
                    setLoading(false);
                }
                else {
                    if (count === 1) {
                        setLoading(true);
                    } else {
                        setTimeout(() => {
                            setLoading(true);
                        }, 5000)
                    }
                }
            }, 6000);
        }

    }


    useEffect(() => {
        getLoading();
    }, [textButton]);

    //Host key function
    const pushResult = () => {
        history.push('/result_game');
    }
    const pushPrew = () => {
        history.push('/');
    }
    useOnKeyPress(startAndStopGame, "Enter");
    useOnKeyPress(pushResult, "n");
    useOnKeyPress(pushPrew, "p");
    const handleChange = async (e) => {
        if (e.target.checked) {
            const manv = e.target.id;
            const update = await updateStatus({ manv: manv });
            if (update.data.success === 'success') {
                console.log('update success co mat');
            } else {
                console.log('Co loi xay ra vui long thu lai!');
            }
        } else {
            const manv = e.target.id;
            const update = await updateStatusDelete({ manv: manv });
            if (update.data.success === 'success') {
                console.log('update success co mat');
            } else {
                console.log('Co loi xay ra khi xoa !');
            }
        }
    }

    return (
        <div className="video-background">
            <ReactHowler
                src={url1}
                playing={playMusic}
                loop={true}
            />
            <ReactHowler
                src={url2}
                playing={!playMusic}
                loop={true}
            />
            <video autoPlay loop muted>
                <source src={url} type="video/mp4" />
                Your browser does not support the video tag.
            </video>
            <div className="content">
                {!loading && (
                    <div className='fix_label' style={labelCss}>
                        <TypeAnimation
                            sequence={[display, 1000]}
                            speed={100}
                            style={{ marginTop: '40px', display: 'block', fontSize: '35px' }}
                            repeat={Infinity}
                            cursor={false}
                        />
                    </div>
                )
                }
                {
                    <div className='fix_center' style={styleCustomer}>
                        {
                            !loading && (
                                <div>
                                    {/* <p>
                                        <TypeAnimation
                                            sequence={[manv, 1000]}
                                            speed={150}
                                            style={{ fontSize: '23px' }}
                                            repeat={Infinity}
                                            cursor={false}
                                        />
                                    </p> */}
                                    <p>

                                        <TypeAnimation
                                            sequence={[hoten, 1000]}
                                            speed={100}
                                            style={{ fontSize: '23px' }}
                                            cursor={false}
                                            repeat={Infinity}
                                        />
                                    </p>
                                </div>
                            )
                        }
                    </div>

                }
                <Confetti
                    className='success_game'
                    config={config}
                    active={!loading} />
                {
                    loading && (
                        <TextLoop
                            className="fix_loading"
                            interval={100}
                            mark={true}

                            springConfig={{ stiffness: 180, damping: 8 }}
                            children={listName}
                        />
                    )

                }
                <button style={styleButton} className='button_play' onClick={startAndStopGame} >
                    {textButton}
                </button>
                <div className='selected_box' style={styleSelectBox}>
                    <select id="dropdown" value={selectedValue} onChange={handleSelectChange}>
                        {options.map((option, index) => (
                            <option key={index} value={option}>
                                {option}
                            </option>
                        ))}
                    </select>
                </div>
                {/* Chỗ này hiển thị list và ô checkbox */}
                <div className="list_checkbox">
                    {
                        !intervalLoop && (
                            listWinner.map((item, index) => {
                                return (
                                    <CFormCheck
                                        key={item.manv}
                                        id={item.manv}
                                        reverse
                                        name={item.manv + item.hoten}
                                        value={item.hoten}
                                        // label={item.manv.toUpperCase() + " - " + item.hoten.toUpperCase() + " - " + item.bophan.toUpperCase() + " - " + item.bp_compact.toUpperCase()}
                                        label={item.hoten.toUpperCase() + " - " + item.bophan.toUpperCase() + " - " + item.bp_compact.toUpperCase()}
                                        onChange={handleChange}
                                    />
                                )
                            })
                        )
                    }

                </div>

                <div
                    style={{
                        position: "absolute",
                        bottom: "-10%",
                        left: "0%",
                        width: "100px",
                        height: "100px",
                    }}
                    onClick={() => { history.push("/") }}

                >
                    {/* prew */}
                </div>
                <div
                    style={{
                        position: "absolute",
                        bottom: "-10%",
                        right: "0%",
                        width: "100px",
                        height: "100px",
                    }}
                    onClick={() => { history.push("/result_game") }}
                >
                </div>
            </div>
        </div>
    )
}

export default PlayGame
