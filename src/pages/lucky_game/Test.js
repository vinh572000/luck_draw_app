import React from 'react'

function Test() {
    const url = `${process.env.PUBLIC_URL}/images/backplay.mp4`;

    return (
        <div className="video-background-test">
            <video autoPlay muted loop>
                <source src={url} type="video/mp4" />
                Your browser does not support the video tag.
                {/* Video sources */}
            </video>
            <div className="content">
                <h1>React Video Background</h1>
                <p>This is some content displayed on top of the video background.</p>
            </div>
        </div>

    )
}

export default Test
